const fs = require('fs');
const path = require('path');
const glob = require('glob');
const R = require('ramda');
const SVGO = require('svgo');
const SVG = require('@davidisaaclee/svg');
const { Point } = require('geometry');
const { parseSVG: parseSVGPath, makeAbsolute } = require('svg-path-parser');

// zipAfterApplying :: ([a] -> [b]) -> [a] -> [[a, b]]
const zipAfterApplying = R.curry((transform, list) => R.zip(list, transform(list)));

// from https://github.com/hughsk/svg-path-parser/issues/11#issuecomment-283196371
function toSVGPath(data) {
  var params = ['rx','ry','xAxisRotation','largeArc','sweep','x1','y1','x2','y2','x','y'];
  var lastCode;
  return data.map(function(cmd){
    var a = [];
    params.forEach(function(param){
      if (param in cmd) {
        var val = cmd[param]*1; // *1 for true/false values on arc
        if (a.length && val>=0) a.push(',');
        a.push(val);
      }
    });
    var result = (lastCode===cmd.code?(a[0]<0?'':','):cmd.code) + a.join('');
    lastCode=cmd.code;
    return result;
  }).join('');
}

// Does not change or return anything; just warns of unclosed paths.
// checkForUnclosedPaths :: SVGPath -> ()
const checkForUnclosedPaths = log => pathData => {
	let startIndex = 0;
	let unclosedPathLocation = SVG.findFirstUnclosedPath(pathData);

	while (unclosedPathLocation != null) {
		log("Unclosed path:", pathData.substr(startIndex + unclosedPathLocation.startIndex, unclosedPathLocation.length));
		startIndex += unclosedPathLocation.startIndex + unclosedPathLocation.length;
		unclosedPathLocation = SVG.findFirstUnclosedPath(pathData.substring(startIndex));
	}
};

function hashPoint({ x, y }) {
	return `{x:${x},y:${y}}`;
}

const isZCommand = command => command.code === 'Z';
const isLCommand = command => command.code === 'L';

// indicesOfRepeatedLCommands :: [Command] -> [Int]
// where [Command] is the result of `parseSVGPath`
function indicesOfRepeatedLCommands(commands) {
	let retval = [];
	let previousCommand = null;

	for (let i = 0; i < commands.length; i++) {
		const command = commands[i];

		if (previousCommand != null && isLCommand(command) && isLCommand(previousCommand) && Point.isEqual(command, previousCommand)) {
			retval.push(i);
		} else {
			previousCommand = command;
		}
	}

	return retval;
}

// indicesOfRedundantLCommandsBeforeClose :: [Command] -> [Int]
function indicesOfRedundantLCommandsBeforeClose(commands) {
	let retval = [];
	let previousCommand = null;

	for (let i = 0; i < commands.length; i++) {
		const command = commands[i];
		if (previousCommand != null && isLCommand(previousCommand) && isZCommand(command) && Point.isEqual(previousCommand, command)) {
			// previousCommand is redundant L command
			retval.push(i - 1);
		}
		previousCommand = command;
	}

	return retval;
}


function indicesOfNoopLCommands(commands) {
	const enumerated = R.addIndex(R.map)((elm, index) => [index, elm]);
	return R.pipe(
		enumerated,
		// Filter out all L commands which travel no distance.
		R.filter(([index, command]) => {
			if (!isLCommand(command)) {
				return false;
			}

			const startPoint = { x: command.x0, y: command.y0 };
			const endPoint = { x: command.x, y: command.y };
			// keep in set if points are equal
			return Point.isEqual(startPoint, endPoint);
		}),
		R.map(([index, _]) => index)
	)(commands);
}

function removeAtIndices(indicesToRemove, array) {
	const indexedFilter = R.addIndex(R.filter);
	return indexedFilter(
		(elm, index) => !R.contains(index, indicesToRemove),
		array);
}

const commandsToIgnoreForRepeatedPointCheck = ['M', 'm', 'Z', 'z'];

// filepath -> bool
let alreadyWarnedAboutRepeatedPoint = {};

const nudgeRepeatedPointsInPath = ({ log, filepath }) => path => {
	return R.pipe(
		parseSVGPath,
		commands => {
			let originalCommands = commands;
			let absoluteCommands = makeAbsolute(R.clone(commands));

			const repeatedLIndices = indicesOfRepeatedLCommands(absoluteCommands);
			absoluteCommands = removeAtIndices(repeatedLIndices, absoluteCommands);
			originalCommands = removeAtIndices(repeatedLIndices, originalCommands);
			
			const redundantLIndicesBeforeClose = indicesOfRedundantLCommandsBeforeClose(absoluteCommands);
			absoluteCommands = removeAtIndices(redundantLIndicesBeforeClose, absoluteCommands);
			originalCommands = removeAtIndices(redundantLIndicesBeforeClose, originalCommands);
			
			const noopLCommandIndices = indicesOfNoopLCommands(absoluteCommands);
			absoluteCommands = removeAtIndices(noopLCommandIndices, absoluteCommands);
			originalCommands = removeAtIndices(noopLCommandIndices, originalCommands);

			return R.zip(originalCommands, absoluteCommands);
		},
		R.reduce(({ visitedPoints, resolvedCommands }, [originalCommand, absoluteCommand]) => {
			if (R.contains(absoluteCommand.code, commandsToIgnoreForRepeatedPointCheck)) {
				return { 
					visitedPoints,
					// resolvedCommands: R.append(absoluteCommand, resolvedCommands) 
					resolvedCommands: R.append(originalCommand, resolvedCommands) 
				};
			}

			let resolvedCommand = absoluteCommand;

			if (!alreadyWarnedAboutRepeatedPoint[filepath] && visitedPoints[hashPoint(resolvedCommand)] != null) {
				alreadyWarnedAboutRepeatedPoint[filepath] = true;

				log('repeated point', 
					resolvedCommand.x,
					resolvedCommand.y,
					visitedPoints[hashPoint(resolvedCommand)],
					originalCommand
				);
			}

			// Don't nudge for now
			// const nudgeDistance = 0.1;
			// let nudgeAngle = 0;
			// let startingPoint = R.pick(['x', 'y'], absoluteCommand);
			// while (visitedPoints[hashPoint(resolvedCommand)] != null) {
			// 	nudgeAngle += 0.1;
			// 	const directionVector =
			// 		{ x: Math.cos(nudgeAngle), y: Math.sin(nudgeAngle) };
			// 	const nudgedPoint =
			// 		Point.add(startingPoint)(Point.scale(nudgeDistance)(Point.normalize(directionVector)));
			// 	resolvedCommand = { ...resolvedCommand, ...nudgedPoint };
			// }

			return {
				visitedPoints: R.assoc(hashPoint(resolvedCommand), originalCommand, visitedPoints),
				//resolvedCommands: R.append(resolvedCommand, resolvedCommands),
				resolvedCommands: R.append(originalCommand, resolvedCommands),
			};
		}, { visitedPoints: {}, resolvedCommands: [] }),
		R.prop('resolvedCommands'),
		toSVGPath
	)(path)
};

// checkForOverlappingPoints :: Logger -> SVGPath -> SVGPath
const checkForOverlappingPoints = ({ log, filepath }) => R.pipe(
	SVG.splitComplexPath,
	R.map(SVG.splitByClosedPaths),
	R.flatten,
	R.map(nudgeRepeatedPointsInPath({ log, filepath })),
	R.join(' '),
);


const svgo = new SVGO({
	full: true,
	plugins: [
		{
			checkPathData: {
				type: 'perItem',
				fn: function(data, _, { path: svgFilePath }) {
					if (data.elem !== 'path') {
						return data;
					}

					if (data.attrs.d == null) {
						return data;
					}

					const log = (...msgs) => console.log(`${path.basename(svgFilePath)}:`, ...msgs);

					data.attrs.d.value = R.pipe(
						R.tap(checkForUnclosedPaths(log)),
						R.tap(checkForOverlappingPoints({ 
							log,
							filepath: svgFilePath
						})),
					)(data.attrs.d.value);
				}
			}
		}
	]
});

require('yargs')
	.command(
		'*',
		'optimize an SVG file',
		yargs => yargs
			.option('i', {
				alias: 'input',
				demandOption: true,
				type: 'string',
			})
			.option('o', {
				alias: 'output',
				demandOption: true,
				type: 'string',
				description: 'output directory for optimized SVGs',
			}),
		({ input: inputPathPattern, output: outputDir }) => {
			glob.sync(inputPathPattern, {}).forEach(inputPath => {
				// console.log(path.basename(inputPath));
				const data = fs.readFileSync(inputPath, 'utf8');

				return svgo.optimize(data, { path: inputPath })
					.then(({ data: optimizedData }) => fs.writeFileSync(path.format({
						dir: outputDir,
						base: path.basename(inputPath)
					}), optimizedData))
					.catch(err => {
						throw err;
					});
			});
		})
	.argv;


const fs = require('fs');
const path = require('path');
const glob = require('glob');
const R = require('ramda');
const { Patterns } = require('../build/dist');

// replacePattern :: (CardModel, PatternID) -> CardModel
function replacePattern(cardModel, patternID) {
	return Object.assign({}, cardModel, {
		backgroundPattern: {
			"type":"reference",
			"identifier": `patterns.${patternID}`,
			"sizeHint":{"x":88.775,"y":57.145}}
	});
}

// copyForAllPatterns :: CardModel -> { PatternID -> CardModel }
function copyForAllPatterns(cardModel) {
	return Object.keys(Patterns.load().all)
		.map(patternID => ({ patternID, cardModel: replacePattern(cardModel, patternID) }))
		.reduce((acc, { patternID, cardModel }) => ({ ...acc, [patternID]: cardModel }), {});
}

require('yargs')
	.command(
		'*',
		'generate a dreamcard from a template for each pattern',
		yargs => yargs
			.option('i', {
				alias: 'input',
				demandOption: true,
				type: 'string',
				description: 'template .dreamcard file',
			})
			.option('o', {
				alias: 'output',
				demandOption: true,
				type: 'string',
				description: 'output directory for all generated dreamcard files',
			}),
		({ input: inputPathPattern, output: outputDir }) => {
			glob.sync(inputPathPattern, {}).forEach(inputPath => {
				const cardModel = JSON.parse(fs.readFileSync(inputPath, 'utf8'));
				const cardModelBasename = path.basename(inputPath, '.dreamcard');

				R.forEachObjIndexed(
					(generatedCardModel, patternID) => {
						const outputPath = path.format({
							dir: outputDir,
							base: `${cardModelBasename}-${patternID}.dreamcard`
						});

						fs.writeFileSync(outputPath, JSON.stringify(generatedCardModel));
					},
					copyForAllPatterns(cardModel)
				);
			});
		})
	.argv;

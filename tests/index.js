import assert from 'assert';
import './installSourceMapSupport';
import { Shape, Graphics, Patterns, defaultEnvironment } from '../dist';

describe("Shape", () => {
	it("should be able to construct pathData shape", () => {
		const size = { x: 15, y: 20 };
		const shape = Shape.pathData({
			paths: ['m 0 0'],
			transform: [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
			size
		});

		assert.deepEqual(shape.size, size);
	});
});

describe("Graphics", () => {
	it("should be able to load graphics", () => {
		const allGraphics = Graphics.load();
		assert.notEqual(allGraphics.all['100'], null);
	});
});

describe("Patterns", () => {
	it("should be able to load patterns", () => {
		const allPatterns = Patterns.load();
		assert.notEqual(allPatterns.all['Celtic'], null);
	});
});

describe("Default environment", () => {
	it("should load", () => {
		const result = defaultEnvironment('graphics.100');
		assert.notEqual(result, undefined);
	});
});


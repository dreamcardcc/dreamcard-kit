'use strict';

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

require('./installSourceMapSupport');

var _dist = require('../dist');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe("Shape", function () {
	it("should be able to construct pathData shape", function () {
		var size = { x: 15, y: 20 };
		var shape = _dist.Shape.pathData({
			paths: ['m 0 0'],
			transform: [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
			size: size
		});

		_assert2.default.deepEqual(shape.size, size);
	});
});

describe("Graphics", function () {
	it("should be able to load graphics", function () {
		var allGraphics = _dist.Graphics.load();
		_assert2.default.notEqual(allGraphics.all['100'], null);
	});
});

describe("Patterns", function () {
	it("should be able to load patterns", function () {
		var allPatterns = _dist.Patterns.load();
		_assert2.default.notEqual(allPatterns.all['Celtic'], null);
	});
});

describe("Default environment", function () {
	it("should load", function () {
		var result = (0, _dist.defaultEnvironment)('graphics.100');
		_assert2.default.notEqual(result, undefined);
	});
});
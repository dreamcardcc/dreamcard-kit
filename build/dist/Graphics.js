'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.load = load;

var _maybes = require('maybes');

var _geometry = require('geometry');

var _svg = require('svg');

var SVG = _interopRequireWildcard(_svg);

var _orderedDictionary = require('ordered-dictionary');

var OrderedDictionary = _interopRequireWildcard(_orderedDictionary);

var _fetched = require('fetched');

var _fetched2 = _interopRequireDefault(_fetched);

var _Shape = require('./Shape');

var Shape = _interopRequireWildcard(_Shape);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function load() {
	var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	var _options$minimumObjec = options.minimumObjectSideLength,
	    minimumObjectSideLength = _options$minimumObjec === undefined ? 100 : _options$minimumObjec;

	// graphics :: OrderedDictionary<ID, Graphics>
	// where Graphics ::= { id :: String, displayName :: String, thumbnail :: Fetched<ImageData>, svg :: Fetched<SVGString> }

	var graphics = OrderedDictionary.fromArray([{ key: '100', value: {} }, { key: 'CelticCorner', value: {} }, { key: 'anger', value: {} }, { key: 'baller', value: {} }, { key: 'ben_franklin', value: {} }, { key: 'black_card', value: {} }, { key: 'bust', value: {} }, { key: 'clubs', value: {} }, { key: 'diamonds', value: {} }, { key: 'disgust', value: {} }, { key: 'dismay', value: {} }, { key: 'elite', value: {} }, { key: 'eye', value: {} }, { key: 'flowers', value: {} }, { key: 'gold_card', value: {} }, { key: 'grimace', value: {} }, { key: 'hearts', value: {} }, { key: 'joy', value: {} }, { key: 'middleschool_s', value: {} }, { key: 'no_mouth', value: {} }, { key: 'permanent_vacation', value: {} }, { key: 'sad', value: {} }, { key: 'spades', value: {} }, { key: 'sweat', value: {} }, { key: 'titanium_card', value: {} }, { key: 'xoxo', value: {} }]);

	graphics.order.forEach(function (key) {
		if (graphics.all[key].displayName == null) {
			graphics.all[key].displayName = key;
		}

		graphics.all[key] = _extends({}, graphics.all[key], graphics.all[key].displayName == null ? { displayName: key } : {}, {
			thumbnail: _fetched2.default.fetching,
			svg: _fetched2.default.fetching
		});
	});

	var idFromFilename = function idFromFilename(suffix) {
		return function (filename) {
			var regex = new RegExp('^.+\\/(.+)' + suffix.source + '$');
			var matchResult = filename.match(regex);
			return matchResult == null ? null : matchResult[1];
		};
	};

	var graphicsIDFromThumbnailFileName = idFromFilename(new RegExp(/_[tT]humb-01\.png/));
	var graphicsIDFromGraphicsFileName = idFromFilename(new RegExp(/_IC\.svg/));

	var thumbnailsLoadContext = require.context("../resources/graphics/thumbnails", true, /^\.\/.*\.png$/);
	thumbnailsLoadContext.keys().forEach(function (fileName) {
		var graphicsID = graphicsIDFromThumbnailFileName(fileName);
		if (graphicsID == null || !(graphicsID in graphics.all)) {
			console.warn('Could not load thumbnail for', fileName);
			return;
		}

		graphics.all[graphicsID].thumbnail = _fetched2.default.fetched(thumbnailsLoadContext(fileName));
	});

	var svgLoadContext = require.context("../resources/graphics", true, /^\.\/.*\.svg$/);
	svgLoadContext.keys().forEach(function (fileName) {
		var graphicsID = graphicsIDFromGraphicsFileName(fileName);
		if (graphicsID == null || !(graphicsID in graphics.all)) {
			console.warn('Could not load SVG for', fileName);
			return;
		}

		var svg = svgLoadContext(fileName);

		var _maybe$map$orJust = (0, _maybes.maybe)(SVG.bounds(svg)).map(function (viewBox) {
			var originalSize = viewBox.size;

			// width = height * aspectRatio 
			// height = width / aspectRatio;
			var aspectRatio = originalSize.x / originalSize.y;
			var maxSideLength = 16;

			var retval = originalSize.x > originalSize.y ? { x: maxSideLength, y: maxSideLength / aspectRatio } : { x: maxSideLength * aspectRatio, y: maxSideLength };

			if (Math.min(retval.x, retval.y) < minimumObjectSideLength) {
				if (retval.x < retval.y) {
					retval = { x: minimumObjectSideLength, y: minimumObjectSideLength * aspectRatio };
				} else {
					retval = { x: minimumObjectSideLength * aspectRatio, y: minimumObjectSideLength };
				}
			}

			return { viewBox: viewBox, desiredSize: retval, scale: retval.x / originalSize.x };
		}).orJust(undefined),
		    viewBox = _maybe$map$orJust.viewBox,
		    desiredSize = _maybe$map$orJust.desiredSize,
		    scale = _maybe$map$orJust.scale;

		var offsetViewBoxToOrigin = _geometry.Transform.translation(_geometry.Point.negate(viewBox.origin));

		graphics.all[graphicsID].svg = _fetched2.default.fetched(Shape.pathData({
			paths: SVG.pathDataFromSVGString(svg),
			size: desiredSize,
			transform: _geometry.Transform.concatenateAll([_geometry.Transform.scaling({ x: scale, y: scale }), offsetViewBoxToOrigin])
		}));
	});

	return graphics;
}
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.size = exports.svgFromShape = exports.reference = exports.pathData = exports.rawSVG = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.makeEnvironment = makeEnvironment;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSvgInline = require('react-svg-inline');

var _reactSvgInline2 = _interopRequireDefault(_reactSvgInline);

var _geometry = require('geometry');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var ShapeType = {
	rawSVG: 'rawSVG',
	pathData: 'pathData',
	reference: 'reference'
};

var rawSVG = exports.rawSVG = function rawSVG(_ref) {
	var svg = _ref.svg,
	    _ref$size = _ref.size,
	    size = _ref$size === undefined ? { x: 16, y: 16 } : _ref$size;
	return {
		type: ShapeType.rawSVG,
		svg: svg,
		size: size
	};
};

var pathData = exports.pathData = function pathData(_ref2) {
	var paths = _ref2.paths,
	    transform = _ref2.transform,
	    _ref2$size = _ref2.size,
	    size = _ref2$size === undefined ? { x: 16, y: 16 } : _ref2$size;
	return {
		type: ShapeType.pathData,
		paths: paths,
		transform: transform,
		size: size
	};
};

// reference :: { identifier :: String, sizeHint :: Point } -> Shape
var reference = exports.reference = function reference(_ref3) {
	var identifier = _ref3.identifier,
	    _ref3$sizeHint = _ref3.sizeHint,
	    sizeHint = _ref3$sizeHint === undefined ? { x: 16, y: 16 } : _ref3$sizeHint;
	return {
		type: ShapeType.reference,
		identifier: identifier,
		sizeHint: sizeHint
	};
};

var Path = function Path(_ref4) {
	var pathData = _ref4.pathData,
	    props = _objectWithoutProperties(_ref4, ['pathData']);

	return _react2.default.createElement('path', _extends({ d: pathData }, props));
};

// svgFromShape :: (Shape, Options, Object, Environment) -> React.Component
// where Options ::= { transform :: Transform }
//       Environment ::= String -> Shape?
//       GroupID ::= String
var svgFromShape = exports.svgFromShape = function svgFromShape(shape) {
	var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	var props = arguments[2];
	var environment = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function () {
		return null;
	};
	var _options$transform = options.transform,
	    transform = _options$transform === undefined ? _geometry.Transform.identity : _options$transform;

	var svgTransform = _geometry.Transform.svgTransformFrom(transform);

	switch (shape.type) {
		case ShapeType.rawSVG:
			return _react2.default.createElement(_reactSvgInline2.default, _extends({
				component: 'g',
				svg: shape.svg,
				svgWidth: '' + shape.size.x,
				svgHeight: '' + shape.size.y,
				transform: svgTransform
			}, props));

		case ShapeType.pathData:
			return _react2.default.createElement(
				'g',
				_extends({
					transform: _geometry.Transform.svgTransformFrom(_geometry.Transform.concatenateAll([transform, shape.transform]))
				}, props),
				shape.paths.map(function (pathData, index) {
					return _react2.default.createElement(Path, { key: index, d: pathData });
				})
			);

		case ShapeType.reference:
			var referencedShape = environment(shape.identifier);
			if (referencedShape == null) {
				return null;
			}

			return _react2.default.createElement(
				'g',
				_extends({ transform: svgTransform }, props),
				svgFromShape(referencedShape, {}, {}, environment)
			);

		default:
			return undefined;
	}
};

// size :: Shape -> Point
var size = exports.size = function size(shape) {
	switch (shape.type) {
		case ShapeType.rawSVG:
			return shape.size;

		case ShapeType.pathData:
			return shape.size;

		case ShapeType.reference:
			return shape.sizeHint;

		default:
			return undefined;
	}
};

// makeEnvironment :: { Identifier -> Shape } -> Environment
// where Identifier ::= String
//       Environment ::= Identifier -> Shape?
function makeEnvironment(dictionary) {
	return function (identifier) {
		return dictionary[identifier];
	};
}
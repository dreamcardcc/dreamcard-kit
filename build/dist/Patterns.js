'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.load = load;

var _fetched = require('fetched');

var _fetched2 = _interopRequireDefault(_fetched);

var _orderedDictionary = require('ordered-dictionary');

var OrderedDictionary = _interopRequireWildcard(_orderedDictionary);

var _Shape = require('./Shape');

var Shape = _interopRequireWildcard(_Shape);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function load() {
	var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	var _options$patternSize = options.patternSize,
	    patternSize = _options$patternSize === undefined ? { x: 100, y: 100 } : _options$patternSize;

	// patterns :: OrderedDictionary<ID, Graphics>
	// where Pattern ::= { id :: String, displayName :: String, thumbnail :: Fetched<ImageData>, svg :: Fetched<SVGString> }

	var patterns = OrderedDictionary.fromArray([{ key: 'CelticCorner', value: {} }, { key: 'Celtic', value: {} }, { key: 'Fern', value: {} }, { key: 'Floral', value: {} }, { key: 'Fractal', value: {} }, { key: 'Greek', value: {} }, { key: 'Hex', value: {} }, { key: 'OpArt', value: {} }, { key: 'Rays', value: {} }, { key: 'Zebra', value: {} }, { key: 'ZigZag', value: {} }, { key: 'banana_leaves', value: {} }, { key: 'dot_grid', value: {} }, { key: 'geo_lattice', value: {} }, { key: 'greek_hearts', value: {} }, { key: 'lv', value: {} }, { key: 'paisley', value: {} }, { key: 'scales', value: {} }, { key: 'squiggles', value: {} }, { key: 'triangle_grid', value: {} }, { key: 'vaporwave', value: {} }, { key: 'wood_grain', value: {} }]);

	patterns.order.forEach(function (key) {
		if (patterns.all[key].displayName == null) {
			patterns.all[key].displayName = key;
		}

		patterns.all[key] = _extends({}, patterns.all[key], patterns.all[key].displayName == null ? { displayName: key } : {}, {
			thumbnail: _fetched2.default.fetching,
			svg: _fetched2.default.fetching
		});
	});

	var idFromFilename = function idFromFilename(suffix) {
		return function (filename) {
			var regex = new RegExp('^.+\\/(.+)' + suffix.source + '$');
			var matchResult = filename.match(regex);
			return matchResult == null ? null : matchResult[1];
		};
	};

	var idFromThumbnailFilename = idFromFilename(new RegExp(/_[Tt]humb\.png/));
	var idFromSVGFilename = idFromFilename(new RegExp(/_BG\.svg/));

	var thumbnailsLoadContext = require.context("../resources/patterns/thumbnails", true, /^\.\/.*\.png$/);
	thumbnailsLoadContext.keys().forEach(function (fileName) {
		var patternID = idFromThumbnailFilename(fileName);
		if (patternID == null || !(patternID in patterns.all)) {
			console.warn('Could not load thumbnail for', fileName);
			return;
		}

		patterns.all[patternID].thumbnail = _fetched2.default.fetched(thumbnailsLoadContext(fileName));
	});

	var svgLoadContext = require.context("../resources/patterns", true, /^\.\/.*\.svg$/);
	svgLoadContext.keys().forEach(function (fileName) {
		var patternID = idFromSVGFilename(fileName);
		if (patternID == null || !(patternID in patterns.all)) {
			console.warn('Could not load SVG for', fileName);
			return;
		}

		patterns.all[patternID].svg = _fetched2.default.fetched(Shape.rawSVG({
			svg: svgLoadContext(fileName),
			size: patternSize
		}));
	});

	return patterns;
}
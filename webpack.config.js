const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: './src/index.js',
  output: {
		path: path.resolve(__dirname, 'build/dist'),
    filename: 'index.js',
    libraryTarget: 'umd',
    library: 'dreamcard-kit'
  },
	module: {
		rules: [
			{
				test: /\.(js)$/,
				use: 'babel-loader'
			},
			{
				test: /\.svg$/,
				use: 'raw-loader',
			},
			{
				oneOf: [
					// "url" loader works like "file" loader except that it embeds assets
					// smaller than specified limit in bytes as data URLs to avoid requests.
					// A missing `test` is equivalent to a match.
					{
						test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
						loader: require.resolve('url-loader'),
						options: {
							// TODO: Make this limit smaller so that we don't store big files in memory
							limit: 10000,
							name: 'static/media/[name].[hash:8].[ext]',
						},
					},
					{
            // Exclude `js` files to keep "css" loader working as it injects
            // it's runtime that would otherwise processed through "file" loader.
            // Also exclude `html` and `json` extensions so they get processed
            // by webpacks internal loaders.
						test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
            loader: require.resolve('file-loader'),
            options: {
              name: 'static/media/[name].[hash:8].[ext]',
            },
          }
				]
			},
		]
	},
  plugins: [
    new webpack.optimize.UglifyJsPlugin()
	],
	devtool: 'source-map',
}

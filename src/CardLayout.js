import { Point } from 'geometry';

// all in mm
let CardLayoutConstants = {
	size: {
		x: 85.60,
		y: 53.97,
	},
	cornerRadius: 3,
	emvChipFrame: {
		origin: { x: 9.4, y: 18.3 },
		size: { x: 10.6, y: 8 },
		cornerRadius: 1.5,
	},
	cardholderNameInset: { x: 2, y: 0 },
	cardholderNameFrame: {
		origin: { x: 9.4, y: 43.97 },
		size: { x: 43.5, y: 4 },
		cornerRadius: 0.5
	},
	bleedMargin: { x: 1.5875, y: 1.5875 },
	signatureStrokeWidth: 0.5,
};

CardLayoutConstants = {
	...CardLayoutConstants,

	sizeWithBleed: Point.add(CardLayoutConstants.size)(Point.scale(2)(CardLayoutConstants.bleedMargin)), 
};

export default CardLayoutConstants;

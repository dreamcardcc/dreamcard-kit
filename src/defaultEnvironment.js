import * as R from 'ramda';

import { load as loadPatterns } from './Patterns';
import { load as loadGraphics } from './Graphics';

// makeEnvironment :: [ShapeGroup] -> Shape.Environment
// where ShapeGroup ::= { prefix :: String, shapes :: { ID -> Shape } }
function makeEnvironmentFromGroups(groups) {
	const dictionary = groups
		.reduce((dict, { prefix, shapes }) => ({
			...dict,
			...R.compose(
				R.fromPairs,
				R.map(([id, shape]) => [`${prefix}${id}`, shape]),
				R.toPairs,
			)(shapes)
		}), {});

	return id => dictionary[id];
}

// TODO: don't hardcode so much
export default makeEnvironmentFromGroups([
	{ 
		prefix: 'graphics.',
		shapes: R.map(g => g.svg.orJust(null), loadGraphics().all)
	},
	// TODO: pattern size
	{
		prefix: 'patterns.',
		shapes: R.map(p => p.svg.orJust(null), loadPatterns().all)
	},
]);


import { maybe } from 'maybes';
import { Point, Transform } from 'geometry';
import * as SVG from '@davidisaaclee/svg';
import * as OrderedDictionary from 'ordered-dictionary';
import Fetched from 'fetched';
import * as Shape from './Shape';

// load :: Options -> OrderedDictionary<String, Graphics>
// where Options ::= { minimumObjectSideLength :: Number }
//       Graphics ::= {
//         id :: String,
//         displayName :: String,
//         isEnabled :: Bool,
//         thumbnail :: Fetched<ImageData>,
//         svg :: Fetched<Shape> 
//       }
export function load(options = {}) {
	// hard-code 3 as EditorDimensions.minimumSideLength :/
	const { minimumObjectSideLength = 3 } = options;

	// graphics :: OrderedDictionary<ID, Graphics>
	// where Graphics ::= { id :: String, displayName :: String, thumbnail :: Fetched<ImageData>, svg :: Fetched<Shape> }
	const graphics = OrderedDictionary.fromArray([
		{ 
			key: '100',
			value: {
				displayName: '100' 
			}
		},
		{ 
			key: 'anger',
			value: {
				displayName: 'Angery' 
			}
		},
		{ 
			key: 'baller',
			value: {
				displayName: 'Baller' 
			}
		},
		{ 
			key: 'ben_franklin',
			value: {
				displayName: 'Benjamin' 
			}
		},
		{ 
			key: 'black_card',
			value: {
				isEnabled: false,
				displayName: 'Black Card' 
			}
		},
		{ 
			key: 'bust',
			value: {
				displayName: 'Bust' 
			}
		},
		{ 
			key: 'CelticCorner',
			value: {
				displayName: 'Celtic Corner' 
			}
		},
		{ 
			key: 'clubs',
			value: {
				displayName: 'Clubs' 
			}
		},
		{ 
			key: 'diamonds',
			value: {
				displayName: 'Diamonds' 
			}
		},
		{ 
			key: 'disgust',
			value: {
				displayName: 'Disgust' 
			}
		},
		{ 
			key: 'dismay',
			value: {
				displayName: 'Dismay' 
			}
		},
		{ 
			key: 'elite',
			value: {
				displayName: 'Elite' 
			}
		},
		{ 
			key: 'eye',
			value: {
				displayName: 'Eye' 
			}
		},
		{ 
			key: 'flowers',
			value: {
				displayName: 'Flowers' 
			}
		},
		{ 
			key: 'gold_card',
			value: {
				isEnabled: false,
				displayName: 'Gold Card' 
			}
		},
		{ 
			key: 'grimace',
			value: {
				displayName: 'Grin' 
			}
		},
		{ 
			key: 'hearts',
			value: {
				displayName: 'Hearts' 
			}
		},
		{ 
			key: 'joy',
			value: {
				displayName: 'Joy' 
			}
		},
		{ 
			key: 'sweat',
			value: {
				displayName: 'Nervous' 
			}
		},
		{ 
			key: 'no_mouth',
			value: {
				displayName: 'No Mouth' 
			}
		},
		{ 
			key: 'sad',
			value: {
				displayName: 'Sad' 
			}
		},
		{ 
			key: 'spades',
			value: {
				displayName: 'Spades' 
			}
		},
		{ 
			key: 'middleschool_s',
			value: {
				displayName: 'Super S' 
			}
		},
		{ 
			key: 'titanium_card',
			value: {
				isEnabled: false,
				displayName: 'Titanium Card' 
			}
		},
		{ 
			key: 'permanent_vacation',
			value: {
				displayName: 'Vacation' 
			}
		},
		{ 
			key: 'xoxo',
			value: {
				displayName: 'XOXO' 
			}
		},
	]);

	graphics.order.forEach(key => {
		if (graphics.all[key].displayName == null) {
			graphics.all[key].displayName = key;
		}

		graphics.all[key] = {
			isEnabled: true,
			thumbnail: Fetched.fetching,
			svg: Fetched.fetching,
			displayName: key,

			...graphics.all[key],
		};
	});

	const idFromFilename = suffix => filename => {
		const regex = new RegExp(`^.+\\/(.+)${suffix.source}$`);
		const matchResult = filename.match(regex);
		return (matchResult == null)
			? null
			: matchResult[1];
	};

	const graphicsIDFromThumbnailFileName = idFromFilename(new RegExp(/_[tT]humb-01\.png/));
	const graphicsIDFromGraphicsFileName = idFromFilename(new RegExp(/_IC\.svg/));

	const thumbnailsLoadContext = require.context("../resources/graphics/thumbnails", true, /^\.\/.*\.png$/);
	thumbnailsLoadContext.keys().forEach(fileName => {
		const graphicsID = graphicsIDFromThumbnailFileName(fileName);
		if ((graphicsID == null) || !(graphicsID in graphics.all)) {
			console.warn('Could not load thumbnail for', fileName);
			return;
		}

		graphics.all[graphicsID].thumbnail = Fetched.fetched(thumbnailsLoadContext(fileName));
	});

	const svgLoadContext = require.context("../resources/graphics", true, /^\.\/.*\.svg$/);
	svgLoadContext.keys().forEach(fileName => {
		const graphicsID = graphicsIDFromGraphicsFileName(fileName);
		if ((graphicsID == null) || !(graphicsID in graphics.all)) {
			console.warn('Could not load SVG for', fileName);
			return;
		}

		const svg = svgLoadContext(fileName);
		const { viewBox, desiredSize, scale } = maybe(SVG.bounds(svg))
			.map(viewBox => {
				const originalSize = viewBox.size;

				// width = height * aspectRatio 
				// height = width / aspectRatio;
				const aspectRatio = originalSize.x / originalSize.y;
				const maxSideLength = 16;

				let retval = originalSize.x > originalSize.y
					? { x: maxSideLength, y: maxSideLength / aspectRatio }
					: { x: maxSideLength * aspectRatio, y: maxSideLength };

				if (Math.min(retval.x, retval.y) < minimumObjectSideLength) {
					if (retval.x < retval.y) {
						retval = { x: minimumObjectSideLength, y: minimumObjectSideLength * aspectRatio };
					} else {
						retval = { x: minimumObjectSideLength * aspectRatio, y: minimumObjectSideLength };
					}
				}

				return { viewBox, desiredSize: retval, scale: retval.x / originalSize.x };
			})
			.orJust(undefined);

		const offsetViewBoxToOrigin =
			Transform.translation(Point.negate(viewBox.origin));

		graphics.all[graphicsID].svg = Fetched.fetched(Shape.pathData({
			paths: SVG.pathDataFromSVGString(svg),
			size: desiredSize,
			transform: Transform.concatenateAll([
				Transform.scaling({ x: scale, y: scale }),
				offsetViewBoxToOrigin,
			]),
		}));
	});

	return graphics;
}


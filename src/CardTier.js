export const CardTier = {
	// (signature OR engraving) AND EMV
	custom: 'tier3',

	// EMV chip or engraving but not both
	moderate: 'tier2',

	// no EMV chip, no engraving
	simple: 'tier1',
};

export function cardTierFromCardInfo({ hasEMVChip, hasEngraving, hasSignature }) {
	if (hasSignature && hasEMVChip) {
		return CardTier.custom;
	} else if (hasEngraving && hasEMVChip) {
		return CardTier.custom;
	} else if (hasEMVChip && !hasEngraving) {
		return CardTier.moderate;
	} else if (hasEMVChip && !hasSignature) {
		return CardTier.moderate;
	} else if (!hasEMVChip && hasSignature) {
		return CardTier.moderate;
	} else if (!hasEMVChip && hasEngraving) {
		return CardTier.moderate
	} else {
		return CardTier.simple;
	}
}


import * as R from 'ramda';

import { CardTier } from './CardTier';
import { type as MetalTypes } from './Metal';
import { variantIDs } from './ShopifyConstants';

function simpleTierNameForCardTier(cardTier) {
	switch (cardTier) {
		case CardTier.simple:
			return 'tier1';
		case CardTier.moderate:
			return 'tier2';
		case CardTier.custom:
			return 'tier3';
		default:
			return undefined;
	}
}

export const InventoryItem = R.indexBy(
	R.identity,
	R.map(
		R.join('-'),
		R.xprod(
			R.values(MetalTypes), 
			R.values(CardTier).map(simpleTierNameForCardTier))));

export function inventoryItemFrom(material, tier) {
	return `${material.name}-${simpleTierNameForCardTier(tier)}`;
}

function materialNameFromVariantID(desiredVariantID) {
	return R.find(({ variantID }) => variantID === desiredVariantID, variantIDs).materialName;
}

function cardTierFromVariantID(desiredVariantID) {
	return R.find(({ variantID }) => variantID === desiredVariantID, variantIDs).cardTier;
}

export function inventoryItemFromVariantID(variantID) {
	return inventoryItemFrom({ name: materialNameFromVariantID(variantID) }, cardTierFromVariantID(variantID));
}


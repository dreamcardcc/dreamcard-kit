import React from 'react';
import { Transform } from 'geometry';

export const ShapeType = {
	image: 'image',
	pathData: 'pathData',
	reference: 'reference',
};

export const image = ({ imageSource, size = { x: 16, y: 16 } }) => ({
	type: ShapeType.image,
	imageSource,
	size
});

export const pathData = ({ paths, transform, size = { x: 16, y: 16 } }) => ({
	type: ShapeType.pathData,
	// remove commas from SVG path string
	paths: paths.map(pathString => pathString.replace(/,/g, ' ')),
	transform,
	size
});


// reference :: { identifier :: String, sizeHint :: Point } -> Shape
export const reference = ({ identifier, sizeHint = { x: 16, y: 16 } }) => ({
	type: ShapeType.reference,
	identifier,
	sizeHint,
});

const Path = ({ pathData, ...props }) => <path d={pathData} {...props} />;

// Resolves referenced shapes.
// If the shape cannot be resolved, returns `null`.
//
// resolve :: (Shape, Environment) -> Shape?
// where Environment ::= String -> Shape?
export const resolve = (shape, environment) => {
	if (shape.type === ShapeType.reference) {
		const referenced = environment(shape.identifier);
		if (referenced == null) {
			return null;
		} else {
			return resolve(referenced, environment);
		}
	} else {
		return shape;
	}
};

// svgFromShape :: (Shape, Options, Object, Environment) -> React.Component
// where Options ::= { transform :: Transform }
//       Environment ::= String -> Shape?
//       GroupID ::= String
export const svgFromShape = (shape, options = {}, props, environment = () => null) => {
	const { transform = Transform.identity } = options;
	const svgTransform = Transform.svgTransformFrom(transform);

	switch (shape.type) {
		case ShapeType.image:
			return (<g 
				transform={Transform.svgTransformFrom(transform)}
				{...props}
			>
				<image 
					width={shape.size.x}
					height={shape.size.y}
					href={shape.imageSource}
					xlinkHref={shape.imageSource}
				/>
			</g>)

		case ShapeType.pathData:
			return (<g 
				transform={Transform.svgTransformFrom(Transform.concatenateAll([transform, shape.transform]))}
				{...props}
			>
				{shape.paths.map((pathData, index) => <Path key={index} pathData={pathData} />)}
			</g>);

		case ShapeType.reference:
			const referencedShape = environment(shape.identifier);
			if (referencedShape == null) {
				return null;
			}

			return (<g transform={svgTransform} {...props}>
				{svgFromShape(referencedShape, {}, {}, environment)}
			</g>);

		default:
			return undefined;
	}
};

// size :: Shape -> Point
export const size = shape => {
	switch (shape.type) {
		case ShapeType.image:
			return shape.size;

		case ShapeType.pathData:
			return shape.size;

		case ShapeType.reference:
			return shape.sizeHint;

		default:
			return undefined;
	}
};

// makeEnvironment :: { Identifier -> Shape } -> Environment
// where Identifier ::= String
//       Environment ::= Identifier -> Shape?
export function makeEnvironment(dictionary) {
	return identifier => dictionary[identifier];
}


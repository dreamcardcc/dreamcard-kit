import * as R from 'ramda';
import { maybe } from 'maybes';
import { CardTier, cardTierFromCardInfo } from './CardTier';
import * as Metal from './Metal';
import * as CardType from './CardType';

const variantIDsByCardTypeCardTierAndMaterial = {
	[CardType.personal]: {
		[CardTier.simple]: {
			[Metal.type.black]: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0Mzk1ODE4',
			[Metal.type.silver]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0Mjk3NTE0",
			[Metal.type.gold]: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0NDk0MTIy',
		},
		[CardTier.moderate]: {
			[Metal.type.black]: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0NDI4NTg2',
			[Metal.type.silver]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0MzMwMjgy",
			[Metal.type.gold]: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0NTI2ODkw',
		},
		[CardTier.custom]: {
			[Metal.type.black]: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0NDYxMzU0',
			[Metal.type.silver]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0MzYzMDUw",
			[Metal.type.gold]: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC83NzY4MzQ0NTU5NjU4',
		},
	},
	[CardType.prepaid]: {
		[CardTier.simple]: {
			[Metal.type.black]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MDI1NjE0Mzc4",
			[Metal.type.silver]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM4MTA2OTIy",
			[Metal.type.gold]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM4MDA4NjE4",
		},
		[CardTier.moderate]: {
			[Metal.type.black]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM3ODc3NTQ2",
			[Metal.type.silver]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM4MTcyNDU4",
			[Metal.type.gold]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM4MDQxMzg2",
		},
		[CardTier.custom]: {
			[Metal.type.black]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM3OTQzMDgy",
			[Metal.type.silver]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM4MjM3OTk0",
			[Metal.type.gold]: "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC85NjY4MTM4MDc0MTU0",
		},
	}
};


// Shopify has two different identifiers for variants. One is only accessible
// via the admin API; the other is only accessible via the storefront API.
// The admin API version is the variant number, and looks like a string of numbers.
// The storefront API version is the variant ID, and is a longer alphanumeric string.
//
// variantNumberToVariantID :: String -> String
export const variantNumberToVariantID = {
	"7768344297514": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.simple][Metal.type.silver],
	"7768344330282": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.moderate][Metal.type.silver],
	"7768344363050": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.custom][Metal.type.silver],

	"7768344395818": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.simple][Metal.type.black],
	"7768344428586": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.moderate][Metal.type.black],
	"7768344461354": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.custom][Metal.type.black],

	"7768344494122": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.simple][Metal.type.gold],
	"7768344526890": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.moderate][Metal.type.gold],
	"7768344559658": variantIDsByCardTypeCardTierAndMaterial[CardType.personal][CardTier.custom][Metal.type.gold],


	"9668138106922": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.simple][Metal.type.silver],
	"9668138172458": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.moderate][Metal.type.silver],
	"9668138237994": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.custom][Metal.type.silver],

	"9668025614378": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.simple][Metal.type.black],
	"9668137877546": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.moderate][Metal.type.black],
	"9668137943082": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.custom][Metal.type.black],

	"9668138008618": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.simple][Metal.type.gold],
	"9668138041386": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.moderate][Metal.type.gold],
	"9668138074154": variantIDsByCardTypeCardTierAndMaterial[CardType.prepaid][CardTier.custom][Metal.type.gold],
};


// variantIDs :: [{
//   variantID :: VariantID,
//   cardType :: CardType,
//   cardTier :: CardTier,
//   materialName :: String,
// }]
export const variantIDs =
	R.compose(
		R.flatten,
		R.map(([cardType, tierToMaterialToVariant]) => R.compose(
			R.flatten,
			R.map(([cardTier, materialToVariant]) => R.compose(
				R.map(([materialName, variantID]) => ({
					variantID,
					cardType,
					cardTier,
					materialName,
				})),
				R.toPairs
			)(materialToVariant)),
			R.toPairs)(tierToMaterialToVariant)),
		R.toPairs,
	)(variantIDsByCardTypeCardTierAndMaterial);

// variantIDForCardInfo :: CardInfo -> String
export const variantIDForCardInfo = cardInfo => {
	const cardTier = cardTierFromCardInfo(cardInfo);
	return variantIDForCardTypeCardTierAndMaterialName({ 
		cardType: cardInfo.type,
		cardTier,
		materialName: cardInfo.design.material.name 
	});
};

export const variantIDForCardTypeCardTierAndMaterialName = ({ cardType, cardTier, materialName }) =>
	variantIDsByCardTypeCardTierAndMaterial[cardType][cardTier][materialName];

export const cardTypeForVariantID =
	desiredVariantID => maybe(variantIDs.find(({ variantID }) => desiredVariantID === variantID))
		.map(({ cardType }) => cardType)
		.orJust(null);


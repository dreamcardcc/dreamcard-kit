import Fetched from 'fetched';
import * as SVG from '@davidisaaclee/svg';
import { maybe } from 'maybes';
import { Transform, Point } from 'geometry';
import * as OrderedDictionary from 'ordered-dictionary';

import * as Shape from './Shape';
import CardLayoutConstants from './CardLayout';

export function load(options = {}) {
	const { patternSize = CardLayoutConstants.sizeWithBleed } = options;
	
	// patterns :: OrderedDictionary<ID, Graphics>
	// where Pattern ::= { id :: String, displayName :: String, thumbnail :: Fetched<ImageData>, svg :: Fetched<SVGString> }
	const patterns = OrderedDictionary.fromArray([
		{
			key: 'CelticCorner',
			value: {
				displayName: 'Celtic',
				isEnabled: false,
			}
		},
		{ 
			key: 'Celtic',
			value: {
				displayName: 'Classical'
			}
		},
		{
			key: 'diamonds',
			value: {
				displayName: 'Diamonds',
			}
		},
		{
			key: 'dot_grid',
			value: {
				displayName: 'Dots',
				isEnabled: false,
			}
		},
		{
			key: 'eighties',
			value: {
				displayName: 'Eighties',
			}
		},
		{
			key: 'lv',
			value: {
				displayName: 'Fancy'
			}
		},
		{
			key: 'Fern',
			value: {
				displayName: 'Fern'
			}
		},
		{
			key: 'Floral',
			value: {
				displayName: 'Floral'
			}
		},
		{
			key: 'Fractal',
			value: {
				displayName: 'Fractal'
			}
		},
		{
			key: 'geo_lattice',
			value: {
				displayName: 'Geo'
			}
		},
		{
			key: 'Greek',
			value: {
				displayName: 'Greco'
			}
		},
		{
			key: 'Hex',
			value: {
				displayName: 'Hex'
			}
		},
		{
			key: 'banana_leaves',
			value: {
				displayName: 'Leaves',
				isEnabled: false,
			}
		},
		{
			key: 'OpArt',
			value: {
				displayName: 'Op Art'
			}
		},
		{
			key: 'greek_hearts',
			value: {
				displayName: 'Ornament'
			}
		},
		{
			key: 'paisley',
			value: {
				displayName: 'Paisley',
				isEnabled: false,
			}
		},
		{
			key: 'Rays',
			value: {
				displayName: 'Rays'
			}
		},
		{
			key: 'rings',
			value: {
				displayName: 'Rings',
			}
		},
		{
			key: 'scales',
			value: {
				displayName: 'Scales',
				isEnabled: false,
			}
		},
		{
			key: 'squiggles',
			value: {
				displayName: 'Scribble'
			}
		},
		{
			key: 'triangle_grid',
			value: {
				displayName: 'Triangles'
			}
		},
		{
			key: 'vaporwave',
			value: {
				displayName: 'Vaporwave',
				isEnabled: false,
			}
		},
		{
			key: 'wood_grain',
			value: {
				displayName: 'Wood Grain'
			}
		},
		{
			key: 'Zebra',
			value: {
				displayName: 'Zebra',
				isEnabled: false,
			}
		},
		{
			key: 'ZigZag',
			value: {
				displayName: 'Zigzag'
			}
		},
	]);

	patterns.order.forEach(key => {
		if (patterns.all[key].displayName == null) {
			patterns.all[key].displayName = key;
		}

		patterns.all[key] = {
			displayName: key,
			thumbnail: Fetched.fetching,
			svg: Fetched.fetching,
			isEnabled: true,
			...patterns.all[key],
		};
	});

	const idFromFilename = suffix => filename => {
		const regex = new RegExp(`^.+\\/(.+)${suffix.source}$`);
		const matchResult = filename.match(regex);
		return (matchResult == null)
			? null
			: matchResult[1];
	};

	const idFromThumbnailFilename = idFromFilename(new RegExp(/_[Tt]humb\.png/));
	const idFromSVGFilename = idFromFilename(new RegExp(/_BG\.svg/));

	const thumbnailsLoadContext = require.context("../resources/patterns/thumbnails", true, /^\.\/.*\.png$/);
	thumbnailsLoadContext.keys().forEach(fileName => {
		const patternID = idFromThumbnailFilename(fileName);
		if ((patternID == null) || !(patternID in patterns.all)) {
			console.warn('Could not load thumbnail for', fileName);
			return;
		}

		patterns.all[patternID].thumbnail = Fetched.fetched(thumbnailsLoadContext(fileName));
	});

	const svgLoadContext = require.context("../resources/patterns", true, /^\.\/.*\.svg$/);
	svgLoadContext.keys().forEach(fileName => {
		const patternID = idFromSVGFilename(fileName);
		if ((patternID == null) || !(patternID in patterns.all)) {
			console.warn('Could not load SVG for', fileName);
			return;
		}

		const svg = svgLoadContext(fileName);
		const { viewBox, scale } = maybe(SVG.bounds(svg))
			.map(viewBox => ({ viewBox, scale: patternSize.x / viewBox.size.x }))
			.orJust(undefined);

		const offsetViewBoxToOrigin =
			Transform.translation(Point.negate(viewBox.origin));
		const positionPatternInCard = Transform.concatenateAll([
			Transform.scaling({ x: scale, y: scale }),
			offsetViewBoxToOrigin,
		]);

		patterns.all[patternID].svg = Fetched.fetched(Shape.pathData({
			paths: SVG.pathDataFromSVGString(svg),
			size: patternSize,
			transform: positionPatternInCard
		}));
	});

	return patterns;
}


import * as Shape from './Shape';
import CardLayoutConstants from './CardLayout';

import blackCard from '../resources/materials/black-card.jpg';
import blackEngraved from '../resources/materials/black-engraved.jpg';
import blackThumbnail from '../resources/materials/thumbnails/black.jpg';
import blackBack from '../resources/materials/black-back.png';
import silverCard from '../resources/materials/silver-card.jpg';
import silverEngraved from '../resources/materials/silver-engraved.jpg';
import silverThumbnail from '../resources/materials/thumbnails/silver.jpg';
import silverBack from '../resources/materials/silver-back.png';
import goldCard from '../resources/materials/gold-card.jpg';
import goldEngraved from '../resources/materials/gold-engraved.jpg';
import goldThumbnail from '../resources/materials/thumbnails/gold.jpg';
import goldBack from '../resources/materials/gold-back.png';

// type :: MetalType
export const type = {
	black: 'black',
	silver: 'silver',
	gold: 'gold',
};


// Metal ::= {
//   name :: MetalType,
//   card :: Shape,
//   engraved :: Shape,
//   thumbnail :: ImageData,
//   back :: ImageData,
//   engravedColor :: CSSColor 
// }
function metal(name, card, engraved, thumbnail, back, engravedColor, outerBorderColor, innerBorderColor) {
	const size = CardLayoutConstants.sizeWithBleed;
	const [cardShape, engravedShape] = [card, engraved]
		.map(data => Shape.image({ imageSource: data, size }));

	return { 
		name,
		card: cardShape,
		engraved: engravedShape,
		thumbnail,
		back,
		engravedColor,
		outerBorderColor,
		innerBorderColor
	};
}

const cardholderNameColor = {
	black: 'white',
	silver: '#535866',
	gold: '#a48433',
};


const cardBorderColors = {
	black: {
		inner: '#000000',
		outer: '#757575',
	},
	silver: {
		inner: '#f4f4f4',
		outer: '#c4c4c4',
	},
	gold: {
		inner: '#ffffa4',
		outer: '#d7cdaa',
	},
};

export const black = metal(
	type.black, blackCard, blackEngraved, blackThumbnail, blackBack, 
	cardholderNameColor.black, cardBorderColors.black.outer, cardBorderColors.black.inner)
export const silver = metal(
	type.silver, silverCard, silverEngraved, silverThumbnail, silverBack,
	cardholderNameColor.silver, cardBorderColors.silver.outer, cardBorderColors.silver.inner)
export const gold = metal(
	type.gold, goldCard, goldEngraved, goldThumbnail, goldBack,
	cardholderNameColor.gold, cardBorderColors.gold.outer, cardBorderColors.gold.inner);

export const priceOfMetal = metal => {
	switch (metal.name) {
		case type.black:
			return 20;
		case type.silver:
			return 30;
		case type.gold:
			return 40;
		default:
			return undefined;
	}
};
